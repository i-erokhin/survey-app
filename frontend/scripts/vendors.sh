#!/usr/bin/env sh

cd "$(dirname "$0")/.." &&

# font-awesome
mkdir -p static/vendors/font-awesome &&
cp -r ../node_modules/font-awesome/css ../node_modules/font-awesome/fonts static/vendors/font-awesome &&

# jquery
mkdir -p static/vendors/jquery &&
cp ../node_modules/jquery/dist/* static/vendors/jquery &&

# tether
mkdir -p static/vendors/tether &&
cp -r ../node_modules/tether/dist/* static/vendors/tether &&

# bootstrap
mkdir -p static/vendors/bootstrap &&
cp -r ../node_modules/bootstrap/dist/* static/vendors/bootstrap

# vue
mkdir -p static/vendors/vue &&
cp -r ../node_modules/vue/dist/vue.js ../node_modules/vue/dist/vue.min.js static/vendors/vue &&

# sortablejs - vor vuedraggable
mkdir -p static/vendors/sortablejs &&
cp -r ../node_modules/sortablejs/Sortable.min.js static/vendors/sortablejs &&

# vuedraggable
mkdir -p static/vendors/vuedraggable &&
cp -r ../node_modules/vuedraggable/dist/vuedraggable.js static/vendors/vuedraggable

cd $OLDPWD
