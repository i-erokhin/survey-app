'use strict';

const path = require('path');
const pug = require('pug');

const isProcuction = process.env.NODE_ENV === 'production';

const tpl_dir = path.resolve(__dirname, 'tpl');
const pug_conf = {
  compileDebug: false,
  pretty: !isProcuction,
  debug: false,
  cache: true,
};

module.exports = async function templateMiddleware(ctx, next) {
  await next();
  if (ctx.template) {
    ctx.locals.url = ctx.url;
    ctx.locals.path = ctx.path;
    ctx.locals.templateDebug = !isProcuction;
    ctx.locals.vendorsMinVersions = isProcuction;
    const startTime = new Date;
    const fn = pug.compileFile(path.resolve(tpl_dir, ctx.template + '.pug'), pug_conf);
    ctx.body = fn(ctx.locals);
    ctx.state.tplRenderTime = new Date - startTime;
  }
};
