exports.pluralForm = pluralForm;

// 1 товар, 2 товара, 5 товаров
function pluralForm(value, one, two, five) {
  value = +value;
  if (value > 20) {
    value %= 10;
  }
  if (value === 1) {
    return one;
  } else if (1 < value && value < 5) {
    return two;
  } else {
    return five;
  }
}
