#!/usr/bin/env sh

DB_NAME="bar"
DB_USER="bar"

cd "$(dirname "$0")" && cd .. &&
cat sql/interfaces-down.sql sql/interfaces-up.sql | psql -U ${DB_USER} ${DB_NAME}
