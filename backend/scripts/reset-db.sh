#!/usr/bin/env sh

DB_NAME="bar"
DB_USER="bar"

cd "$(dirname "$0")" && cd .. &&
cat sql/schema-down.sql sql/schema-up.sql sql/interfaces-up.sql sql/seeds.sql | psql -U ${DB_USER} ${DB_NAME}
