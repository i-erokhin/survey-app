'use strict';

const Router = require('koa-router');
const router = module.exports = new Router();

router.post('/answer/:shopId/:questionId', answer);

async function answer(ctx) {
  let q = `INSERT INTO surveys.answer (member_id, shop_id, question_id, score) VALUES ($1, $2, $3, $4)`;
  await ctx.pool.query(q, [
    ctx.session.member_id,
    ctx.params.shopId,
    ctx.params.questionId,
    ctx.request.body.score
  ]);
  ctx.body = 'Ok';
}
