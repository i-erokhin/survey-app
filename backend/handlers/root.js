'use strict';

const Router = require('koa-router');
const router = module.exports = new Router();

router.get('/', rootPage);
router.post('login', '/', login);
// todo: logout by POST method
router.get('/logout', logout);
router.get('shop', '/:shopSlug', shop);

async function helperGetDefaultShopUrl(pool) {
  const q = `SELECT slug FROM shops.shop ORDER BY name LIMIT 1`;
  const slug = (await pool.query(q)).rows[0].slug;
  // todo: if no shops in db - what to do?
  return router.url('shop', slug);
}

async function rootPage(ctx) {
  if (ctx.session.member_id === undefined) {
    ctx.template = 'login';
  } else {
    const defaultShopUrl = await helperGetDefaultShopUrl(ctx.pool);
    ctx.redirect(defaultShopUrl);
  }
}

async function login(ctx) {
  const q = `SELECT id FROM members.auth($1::TEXT, $2::TEXT)`;
  const member = (await ctx.pool.query(q, [ctx.request.body.email, ctx.request.body.password])).rows[0];
  if (member === undefined) {
    // todo: login error message
    ctx.locals.email = ctx.request.body.email;
    ctx.template = 'login';
  } else {
    ctx.session.member_id = member.id;
    const defaultShopUrl = await helperGetDefaultShopUrl(ctx.pool);
    ctx.redirect(defaultShopUrl);
  }
}

async function logout(ctx) {
  ctx.session.member_id = undefined;
  ctx.redirect(router.url('login'));
}

async function shop(ctx) {
  const q = `SELECT id, name, slug FROM shops.shop WHERE slug = $1`;
  ctx.locals.currentShop = (await ctx.pool.query(q, [ctx.params.shopSlug])).rows[0];
  // todo: 404 page
  ctx.assert(ctx.locals.currentShop, 404);
  ctx.template = 'base/shop';
  const q2 = `SELECT id, name, slug FROM shops.shop ORDER BY name`;
  ctx.locals.shops = (await ctx.pool.query(q2)).rows;
  // questions with no answers for current shop from current user
  const q3 = `SELECT * FROM surveys.get_unanswered_questions($1::INT, $2::INT)`;
  ctx.locals.questions = (await ctx.pool.query(q3, [ctx.session.member_id, ctx.locals.currentShop.id])).rows;
}
