'use strict';

const Router = require('koa-router');
const router = module.exports = new Router();
const DB_ERROR_CODE_DUPLICATE_ENTRY = '23505';
const ERROR_MESSAGE_DUPLICATE_ENTRY = 'Error: Question with the same title already exist.';

router.all('*', adminMiddleware);
router.get('index', '/', index);
router.put('/sort', sort);
router.post('/', create);
router.get('/:id', read);
router.put('/:id', update);
router.post('/:id/delete', del);

async function adminMiddleware(ctx, next) {
  if (!ctx.locals.member.is_admin) {
    ctx.redirect('/');
  } else {
    await next();
  }
}

async function index(ctx) {
  ctx.template = 'base/admin/list';
  const q = `SELECT * FROM surveys.question_answers_count`;
  ctx.locals.questions = (await ctx.pool.query(q)).rows;
}

async function sort(ctx) {
  const sort = JSON.parse(ctx.request.body.sort);
  for (let i = 0; i < sort.length; i++) {
    // todo: db routine
    let q = `UPDATE surveys.question SET sort = $1 WHERE id = $2`;
    await ctx.pool.query(q, [i, sort[i]]);
  }
  ctx.body = 'Ok';
}

async function create(ctx) {
  const q = `UPDATE surveys.question SET sort = sort + 1`;
  await ctx.pool.query(q);
  const q2 = `INSERT INTO surveys.question (title) VALUES ($1) RETURNING *`;
  try {
    ctx.body = (await ctx.pool.query(q2, [ctx.request.body.title.trim()])).rows[0];
  } catch (e) {
    if (e.code === DB_ERROR_CODE_DUPLICATE_ENTRY) {
      ctx.body = ERROR_MESSAGE_DUPLICATE_ENTRY;
      ctx.status = 409;
    } else {
      throw e;
    }
  }
}

async function read(ctx) {
  ctx.template = 'base/admin/item';
  const q = `SELECT * FROM surveys.question WHERE id = $1`;
  ctx.locals.question = (await ctx.pool.query(q, [ctx.params.id])).rows[0];
  ctx.assert(ctx.locals.question, 404);

  ctx.locals.answers = {};

  const q2 = `SELECT score, count(*) FROM surveys.answer WHERE question_id = $1 GROUP BY question_id, score ORDER BY score`;
  ctx.locals.answers.total = (await ctx.pool.query(q2, [ctx.locals.question.id])).rows;
  ctx.locals.answers.totalSum = 0;
  for (let i of ctx.locals.answers.total) {
    ctx.locals.answers.totalSum += Number(i.count);
  }
}

async function update(ctx) {
  const q = `UPDATE surveys.question SET title = $1 WHERE id = $2 RETURNING *`;
  ctx.body = (await ctx.pool.query(q, [ctx.request.body.title.trim(), ctx.params.id])).rows[0];
}

async function del(ctx) {
  const q = `DELETE FROM surveys.question WHERE id = $1`;
  await ctx.pool.query(q, [ctx.params.id]);
  ctx.redirect(router.url('index'));
}
