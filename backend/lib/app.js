'use strict';

const path = require('path');
const Koa = require('koa');
const statik = require('koa-static');
const session = require('koa-session2');
const bodyParser = require('koa-bodyparser');
const Pool = require('pg-pool');
const winston = require('winston');
const middleware = require('./middleware');
const RedisStore = require('./session-store-redis');
const frontendMiddleware = require('../../frontend/template-middleware');

const rootRouter = require('../handlers/root');
const surveyRouter = require('../handlers/survey');
const adminRouter = require('../handlers/admin');

const app = module.exports = new Koa();

const isProcuction = process.env.NODE_ENV === 'production';

/*** app.context SETUP ***/

app.context.pool = new Pool({
  database: 'bar',
  user: 'bar',
  password: 'bar',
  host: 'localhost',
  port: 5432,
  ssl: true,
  max: 10,
  min: 1,
  idleTimeoutMillis: 1000
});

app.context.log = new winston.Logger({transports: [
  new winston.transports.Console({
    timestamp: true,
    colorize: true,
    level: 'debug'
  })
]});

app.context.conf = {
  host: isProcuction ? '0.0.0.0' : 'localhost',
  port: 3000
};

app.context.locals = {};
app.context.template = '';

/*** MIDDLEWARE CHAIN ***/

const staticMiddleware = statik(path.resolve(__dirname, '..', '..', 'frontend', 'static'), {maxage: 86400 * 31});
const sessionMiddleware = session({key: 'sid', store: new RedisStore()});

app.use(middleware.root);
app.use(staticMiddleware);
app.use(sessionMiddleware);
app.use(bodyParser());
app.use(frontendMiddleware);
app.use(middleware.member);

/*** ROUTERS ***/

surveyRouter.prefix('/survey');
app
  .use(surveyRouter.routes())
  .use(surveyRouter.allowedMethods());

adminRouter.prefix('/admin');
app
  .use(adminRouter.routes())
  .use(adminRouter.allowedMethods());

app
  .use(rootRouter.routes())
  .use(rootRouter.allowedMethods());
