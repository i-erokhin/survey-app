'use strict';

/**
 * Requests logger with timing, include template render time separately.
 *
 * @param ctx
 * @param next
 * @returns {Promise.<void>}
 */
exports.root = async function root(ctx, next) {
  const start = new Date;
  let middlewareError;
  try {
    await next();
  } catch (e) {
    middlewareError = e;
  }

  const ms = new Date - start;
  ctx.set('X-Response-Time', ms + 'ms');

  if (middlewareError || !ctx.body) {
    ctx.log.error(`${ms} ${ctx.method} ${ctx.status} ${ctx.url}`);
    if (middlewareError) {
      throw middlewareError;
    }
  } else {
    const httpString = `${ctx.method} ${ctx.status} ${ctx.url}`;
    if (ctx.state.tplRenderTime !== undefined) {
      ctx.log.info(`${ms} ${ctx.state.tplRenderTime} ${httpString}`);
    } else {
      ctx.log.info(`${ms} ${httpString}`);
    }
  }
};

/**
 * Get member from db by "member_id" from session. Member id is permanent, other member params
 * can be changed in real-time, by another admin for instance.
 *
 * @param ctx
 * @param next
 * @returns {Promise.<void>}
 */
exports.member = async function member(ctx, next) {
  let member = null;
  ctx.locals.member = null;
  if (ctx.session.member_id) {
    const q = `SELECT id, email, is_admin FROM members.member WHERE id = $1`;
    member = (await ctx.pool.query(q, [ctx.session.member_id])).rows[0];
    if (member === undefined) {
      ctx.session.member_id = undefined;
    }
  }

  if (member) {
    ctx.locals.member = member;
    await next();
  } else {
    if (ctx.url === '/') {
      await next();
    } else {
      ctx.redirect('/');
    }
  }
};
