CREATE SCHEMA members;

CREATE TABLE members.member (
    id       SERIAL PRIMARY KEY,
    email    VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(30)  NOT NULL,
    is_admin BOOLEAN      NOT NULL DEFAULT FALSE,
    ctime    TIMESTAMP    NOT NULL DEFAULT now()
);

CREATE SCHEMA shops;

CREATE TABLE shops.shop (
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(100) NOT NULL UNIQUE,
    slug    VARCHAR(100) NOT NULL UNIQUE
);

CREATE SCHEMA surveys;

CREATE TABLE surveys.question (
    id      SERIAL PRIMARY KEY,
    title   TEXT NOT NULL UNIQUE,
    sort    INT NOT NULL DEFAULT 0
);

CREATE TYPE surveys.score AS ENUM ('1', '2', '3');

CREATE TABLE surveys.answer (
    member_id   INT NOT NULL REFERENCES members.member (id) ON DELETE CASCADE,
    shop_id     INT NOT NULL REFERENCES shops.shop (id) ON DELETE CASCADE,
    question_id INT NOT NULL REFERENCES surveys.question ON DELETE CASCADE,
    score       surveys.score NOT NULL,
    ctime       TIMESTAMP    NOT NULL DEFAULT now(),
    PRIMARY KEY (member_id, shop_id, question_id)
);
