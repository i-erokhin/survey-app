-- drop all views

DO $$
DECLARE
  r RECORD;
  s TEXT;
BEGIN
  FOR r IN SELECT
             table_schema,
             table_name
           FROM information_schema.views
           WHERE table_schema NOT IN ('pg_catalog', 'information_schema')
  LOOP
    s := 'DROP VIEW IF EXISTS ' || quote_ident(r.table_schema) || '.' || quote_ident(r.table_name) || ' CASCADE;';

    EXECUTE s;
    RAISE NOTICE 's = % ', s;
  END LOOP;
END$$;

-- drop all routines

DO $$
DECLARE
  r RECORD;
  s TEXT;
BEGIN
  FOR r IN SELECT
             nspname,
             proname,
             proargtypes
           FROM
             pg_proc p,
             pg_language l,
             pg_type t,
             pg_namespace n
           WHERE
             p.prolang = l.oid
             AND p.prorettype = t.oid

             AND p.pronamespace = n.oid
             AND l.lanname IN ('sql', 'plpgsql')
             AND nspname NOT IN ('information_schema', 'pg_catalog')
  LOOP
    s :=
    'DROP FUNCTION IF EXISTS '
    || quote_ident(r.nspname)
    || '.'
    || r.proname
    || '('
    || oidvectortypes(r.proargtypes)
    || ') CASCADE;';

    EXECUTE s;
    RAISE NOTICE 's = % ', s;
  END LOOP;
END$$;
