CREATE FUNCTION members.auth(_email TEXT, _password TEXT)
    RETURNS TABLE(id INT, email TEXT, is_admin BOOL)
AS $$
    SELECT id, email, is_admin
    FROM members.member
    WHERE email = _email AND password = _password;
$$ LANGUAGE SQL;


CREATE FUNCTION surveys.get_unanswered_questions(_member_id INT, _shop_id INT)
    RETURNS TABLE(id INT, title TEXT)
AS $$
    SELECT id, title
    FROM surveys.question
    LEFT JOIN
        (
            SELECT question_id
            FROM surveys.answer
            WHERE member_id = _member_id AND shop_id = _shop_id
        ) answer
        ON surveys.question.id = answer.question_id
    WHERE answer.question_id IS NULL
    ORDER BY sort;
$$ LANGUAGE SQL;


CREATE VIEW surveys.question_answers_count AS
  SELECT
    id,
    COALESCE(answers_count, 0) answers_count,
    title
  FROM surveys.question
    LEFT JOIN (SELECT
                 question_id,
                 count(*) answers_count
               FROM surveys.answer
               GROUP BY question_id) a ON surveys.question.id = a.question_id
  ORDER BY sort;

