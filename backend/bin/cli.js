#!/usr/bin/env node
'use strict';

const app = require('../lib/app');

app.listen(app.context.conf.port, app.context.conf.host);
app.context.log.warn(`Listening on http://${app.context.conf.host}:${app.context.conf.port}/`);
